
public void loadModel() {
  values = loadJSONArray("data/pyramids.json");
  
  for (int i=0; i<values.size(); i++){
  
    JSONObject pyramid = values.getJSONObject(i);
   
    Point a_new = new Point(pyramid.getFloat("Point a_x"),pyramid.getFloat("Point a_y"),pyramid.getFloat("Point a_z"));
    Point b_new = new Point(pyramid.getFloat("Point b_x"),pyramid.getFloat("Point b_y"),pyramid.getFloat("Point b_z"));
    Point c_new = new Point(pyramid.getFloat("Point c_x"),pyramid.getFloat("Point c_y"),pyramid.getFloat("Point c_z"));
    Point top_new = new Point(pyramid.getFloat("Point Top_x"),pyramid.getFloat("Point Top_y"),pyramid.getFloat("Point Top_z"));
    
    myPyramids.add(new Pyramid_S3(a_new, b_new, c_new, top_new));
    
    
    
  }

}
public void saveModel() {
  values = new JSONArray();
  for (int i = 0; i < myPyramids.size(); i++) {

    JSONObject pyramid = new JSONObject();
    pyramid.setInt("id", i);
    pyramid.setFloat("Point a_x", myPyramids.get(i).a.x);
    pyramid.setFloat("Point a_y", myPyramids.get(i).a.y);
    pyramid.setFloat("Point a_z", myPyramids.get(i).a.z);
    pyramid.setFloat("Point b_x", myPyramids.get(i).b.x);
    pyramid.setFloat("Point b_y", myPyramids.get(i).b.y);
    pyramid.setFloat("Point b_z", myPyramids.get(i).b.z);
    pyramid.setFloat("Point c_x", myPyramids.get(i).c.x);
    pyramid.setFloat("Point c_y", myPyramids.get(i).c.y);
    pyramid.setFloat("Point c_z", myPyramids.get(i).c.z);
    pyramid.setFloat("Point Top_x", myPyramids.get(i).top.x);
    pyramid.setFloat("Point Top_y", myPyramids.get(i).top.y);
    pyramid.setFloat("Point Top_z", myPyramids.get(i).top.z);
    
    values.setJSONObject(i, pyramid);
  }
  saveJSONArray(values, "data/pyramids.json");
}
