void draw_layer_1() {
  background(0);
  //lights();
  spotlights();

  pointlight();
  pointlight2();
  if (pyramidpoints_visible == true) {
    show_number_of_new_pyramidpoints();
  }

  draw_ballons();

}

void spotlights(){
  int i = 0;
  for(Pyramid_S3 actual_py : myPyramids){
    if(i<6){
    spotLight(51, 102, 126, actual_py.a.x, actual_py.a.y, 50, 0, 0, -1, PI/2, 2);
    i++;
    }

  }

}

void show_number_of_new_pyramidpoints() {
  textSize(32);
  text(new_pyramid_points.size(), 10, 30);
}

void pointlight() {
  int x;
  int y;
  float angle = radians(frameCount);
  float radius = 350;
  x = int(radius*cos(angle)+width/2);
  y = int(radius*sin(angle)+height/2);
  pointLight(255*sin(frameCount/1000), 255*cos(frameCount/5000), 200*cos(frameCount/3000+2000), x, y, 300);
}
void pointlight2() {
  int x;
  int y;
  float angle = radians(frameCount*0.7);
  float radius = 500;
  x = int(radius*cos(angle)+width/2);
  y = int(radius*sin(angle)+height/2);
  pointLight(255-255*sin(frameCount/1000), 255-255*cos(frameCount/5000), 255-200*cos(frameCount/3000+2000), x, y, 300);
}

void draw_ballons(){
  for (int i = 0; i<myBalloons.size(); i++)
  {
    // instead of myBalloons[i], ArrayList elements are adressed with myBalloons.get(i)
    //println("baloon" + i);
    myBalloons.get(i).update(i);
    //delete balloons with the mouse (right click)
    if (myBalloons.get(i).isHit()) {
      myBalloons.remove(i);
    }
  }
}
