// --- load global objects

// ArrayList with Pyramids which are shown on screen
ArrayList<Pyramid_S3> myPyramids = new ArrayList<Pyramid_S3>();
// Arraylist with new Points which will create next Pyramid
ArrayList<Point> new_pyramid_points = new ArrayList<Point>();

ArrayList<Balloon> myBalloons = new ArrayList<Balloon>();



JSONArray values;

boolean pyramidpoints_visible;
boolean building_pyramid = false;
boolean deleting_possible = true;
boolean finding_next_points_allowed = false;
boolean next_to_point;
boolean far_away;
Point point_to_save;
boolean point_to_save_found = false;
