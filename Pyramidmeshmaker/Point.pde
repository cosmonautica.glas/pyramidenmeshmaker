class Point {

  float x, y, z;
  color corner_color = color (255,000,255);

  Point(float _x, float _y, float _z){
  x = _x;
  y = _y;
  z = _z;

  }

  float get_x(){return x;}
  float get_y(){return y;}
  float get_z(){return z;}

  void update()
  {
    drawSelf();
  }

  void drawSelf(){
  pushMatrix();
  strokeWeight(3);
  stroke(corner_color);
  point(x,y,z);
  popMatrix();
  }
}
