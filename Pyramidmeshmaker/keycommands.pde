void keyTyped() {
  if (key == 'S') {
    saveModel();
    return;
  }
  if (key == 'L') {
    loadModel();
    return;
  }
  if (key == 't' && next_to_point == true){
    new_pyramid_points.add(point_to_save);
    return;
  }
  if (key == 'D' && myPyramids.size()>0 && pyramidpoints_left == false ) {
    myPyramids.remove(myPyramids.size()-1);
    return;
  }
  if (key =='d' && pyramidpoints_left == true) {
    new_pyramid_points.remove(new_pyramid_points.size()-1);
    return;
  }
  if (key == 'C' ) {
    display_p5 = true;
    return;
  }
  if (key =='c') {
    display_p5 = false;
    return;
  }
  if (key == 'B' ) {
      ballons_active = true;
      return;
    }
    if (key =='b') {
      ballons_active = false;    
      return;
    }
  if (key == 'q' && pyramidpoints_visible == false) {
    pyramidpoints_visible = true;
    return;
  }
  else if (key == 'q' && pyramidpoints_visible == true) {
    pyramidpoints_visible = false;
    return;
  }
  if (key == 'p' && finding_next_points_allowed == false) {
    finding_next_points_allowed = true;
    println("finding points allowed");
    return;
  }
  if (key == 'p' && finding_next_points_allowed == true) {
    finding_next_points_allowed = false;
    println("finding points forbidden");
    return;
  }
}

void keyPressed() {

  if (key == 'm') {
    println(mouseX);
    println(mouseY);
  }
}
