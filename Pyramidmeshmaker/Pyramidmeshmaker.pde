import controlP5.*;
ControlP5 controlP5;
controlP5.Numberbox pyramidheight_box;



void setup() {
  size(800, 800, P3D);
  //  camera(width/2.0, height/2.0, 520.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0);
  println("setup");

  controlP5 = new ControlP5(this);
  pyramidheight_box =controlP5.addNumberbox("heightofpy")
                              .setPosition(10,60)
                              .setSize(150,20)
                              .setRange(0,100)
                              .setScrollSensitivity(1.1)
                              .setValue(25);

  initBalloons(15);





/*
  cp5.addNumberbox("pyramidheight")
   .setPosition(10,60)
   .setSize(150,20)
   .setRange(0,50)
   .setScrollSensitivity(1.1)
   .setValue(25)
   ;
*/
}

void draw() {

  if (new_pyramid_points.size()>0) {
    pyramidpoints_left = true;
  } else {
    pyramidpoints_left = false;
  }
  if (finding_next_points_allowed == true) {
    next_to_point();
    if (next_to_point == true) {
      println("next_to_point");
    }
  }

  //---------Layer_1 Background------
  draw_layer_1();
  //---------------------------------
  //---------Layer_2 Pyramids--------
  draw_layer_2();
  //---------------------------------
  //---------Layer_3 HighLights------
  draw_layer_3();
  //---------------------------------
}


void mouseClicked() {
  //println("initPyramids");

  if ((mouseButton == LEFT) && new_pyramid_points.size() != 3 && ballons_active == false) {
      new_pyramid_points.add(new Point(mouseX, mouseY, 0));
      println(new_pyramid_points.size());
      println(mouseX);
      println(mouseY);
      if (new_pyramid_points.size() ==3){mousepoint_visible = true;}
  }
  else if ((mouseButton == LEFT && new_pyramid_points.size() == 3)&& ballons_active == false) {
 //     new_pyramid_points.add(new Point(mouseX, mouseY, pyramidheight.get_value()));
      new_pyramid_points.add(new Point(mouseX, mouseY, heightofpy));
      println(new_pyramid_points.size());
      println(mouseX);
      println(mouseY);
      myPyramids.add(new Pyramid_S3(new_pyramid_points.get(0), new_pyramid_points.get(1), new_pyramid_points.get(2), new_pyramid_points.get(3)));

      for (int i = new_pyramid_points.size() - 1; i >= 0; i--) {
        new_pyramid_points.remove(i);
      }
      mousepoint_visible = false;
      println("Pyramidpoints converted to Pyramid");
    }

    if (mouseButton == LEFT && ballons_active == true) {
  // create a new balloon upon mouseclick
  myBalloons.add(new Balloon(mouseX, mouseY, 2, color(random(255), random(255), random(255))));
  }
}
/*
void set_top_height(){
  int x =0;
    println("Please enter height in relation to last edge width. Type as float and press ENTER");

    if(keyPressed = ENTER)
}
*/
void next_to_point() {
  for (int i = 0; i < myPyramids.size(); i++) {
    if (   myPyramids.get(i).get_a().get_x() > mouseX-3
      &&   myPyramids.get(i).get_a().get_x() < mouseX+3
      &&   myPyramids.get(i).get_a().get_y() > mouseY-3
      &&   myPyramids.get(i).get_a().get_y() < mouseY+3) {
      next_to_point = true;
      point_to_save = myPyramids.get(i).get_a();
      return;
    } else if (  myPyramids.get(i).get_b().get_x() > mouseX-3
      &&       myPyramids.get(i).get_b().get_x() < mouseX+3
      &&       myPyramids.get(i).get_b().get_y() > mouseY-3
      &&       myPyramids.get(i).get_b().get_y() < mouseY+3) {
      next_to_point = true;
      point_to_save = myPyramids.get(i).get_b();
      return;
    } else if (myPyramids.get(i).get_c().get_x() > mouseX-3
      && myPyramids.get(i).get_c().get_x() < mouseX+3
      && myPyramids.get(i).get_c().get_y() > mouseY-3
      && myPyramids.get(i).get_c().get_y() < mouseY+3) {
      next_to_point = true;
      point_to_save = myPyramids.get(i).get_c();
      return;
    } else {
      next_to_point = false;

    }
  }
}

void initBalloons(int num) {
  for (int i = 0; i<num; i++) {
//    Balloon new_one = new Balloon(random(width), random(height), 2, color(random(255), random(255), random(255)));
    Balloon new_one = new Balloon(random(width), random(height), 2);
    if (new_one.intersect_ball(myBalloons.size()-1) == false) {
      //myBalloons.add(new_one);
      myBalloons.add(new_one);
      //myBalloons.add(new Balloon(random(width), random(height), 2, color(random(255), random(255), random(255))));
    }
  }
}
