void draw_layer_2() {
 // draw_bottom_layers();
  draw_side_layers_of_pyramids();
  draw_meshes();
  draw_construction_points();
  //draw_walkler();
}
void draw_side_layers_of_pyramids() {

  for (int i = 0; i < myPyramids.size(); i++) {
    myPyramids.get(i).drawsidelayers();
  }
}
void draw_bottom_layers() {

  for (int i = 0; i < myPyramids.size(); i++) {
    myPyramids.get(i).drawbottomlayer();
  }
}

void draw_meshes() {
  for (int i = 0; i < myPyramids.size(); i++) {
    myPyramids.get(i).drawSelf();
  }
}
void draw_construction_points() {
  for (int i = 0; i < new_pyramid_points.size(); i++) {
    new_pyramid_points.get(i).update();
  }
}
