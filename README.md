This Pyramidmeshmaker should make it easy to you, 
to create a meshnet of Triangle-Pyramids,
which can be mapped by beamer projection.

Next Ideas for Implementation are:
- pyramids with square
- correction of top-point-position
- re-editing of pyramids
- sureface surface effects on pyramids


For using the Meshmaker:

1.      Install Processing from https://processing.org/
        and download the git-repository.


2.      Start Pyramidmeshmaker.pde 



Commands while use

Press q: You see/(not see) the number of Points of your next Pyramidmesh


I am very interested in comments and ideas.
Please write me at cosmonautica.glas@gmail.com

